import { Component, ChangeDetectionStrategy } from '@angular/core';

import { ToastyConfig } from 'ng2-toasty';
import { Angular2TokenService } from 'angular2-token';

@Component({
  selector: 'app-root',
  templateUrl: './app.page.html',
  styleUrls: ['./app.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppPage {
  constructor(
    private tokenService: Angular2TokenService,
    private toastyConfig: ToastyConfig,
  ) {
    this.tokenService.init({
      apiPath:               'api/edge',
    });

    // configure Toasty
    this.toastyConfig.theme    = 'bootstrap';
    this.toastyConfig.position = 'top-right';
  }
}
