import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

// import { Project } from 'app/core/models';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TasksComponent implements OnInit {
  // @Input() project: Project;

  constructor() { }

  ngOnInit() {
  }

}
