import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Angular2TokenService } from 'angular2-token';

import { HomePage } from './home.page';

const homeRoutes: Routes = [
  { path: '', component: HomePage, canActivate: [Angular2TokenService] },
];

@NgModule({
  imports: [
    RouterModule.forChild(homeRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeRouting { }
