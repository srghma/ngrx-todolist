// angular
import { Component, OnInit, AfterViewInit } from '@angular/core';

// libs
import { Apollo, ApolloQueryObservable } from 'apollo-angular';
import { ApolloQueryResult } from 'apollo-client';
import { Subject } from 'rxjs/Subject';
import { DocumentNode } from 'graphql';

// app
import { ProjectsQuery, CreateProjectMutation } from 'graphql/schema';
const ProjectsQueryNode: DocumentNode = require('graphql-tag/loader!../graphql/Projects.graphql');
const CreateProjectMutationNode: DocumentNode = require('graphql-tag/loader!../graphql/CreateProject.graphql');

@Component({
  selector:        'app-home',
  templateUrl:     './home.page.html',
  styleUrls:       ['./home.page.scss']
})
export class HomePage {
  projects$: ApolloQueryObservable<ProjectsQuery>;

  constructor(
    private apollo: Apollo
  ) {
  }

  public ngOnInit() {
    // Query users data with observable variables
    this.projects$ = this.apollo.watchQuery<ProjectsQuery>({
      query: ProjectsQueryNode,
    })
      .map(result => result.data.projects) as any;
  }

  public newProject(name: string) {
    // Call the mutation called addUser
    this.apollo.mutate<CreateProjectMutation>({
      mutation: CreateProjectMutationNode,
      variables: {
        name,
      },
    })
      .subscribe({
        next: ({data}) => {
          console.log('got a new user', data);

          // get new data
          // this.users.refetch();
        },
        error: (errors) => {
          console.log('there was an error sending the query', errors);
        }
      });
  }

}
