// angular
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

// app
// import { Project } from 'app/core/models';

@Component({
  selector:        'app-projects',
  templateUrl:     './projects.component.html',
  styleUrls:       ['./projects.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectsComponent implements OnInit {
  // projects: Project[];
  editing: boolean;

  constructor() { }

  ngOnInit() {
  }

  public create() {}

}
