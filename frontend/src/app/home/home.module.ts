// angular
import { NgModule }     from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule }  from '@angular/forms';

// app
import { HomeRouting }       from './home.routing';
import { HomePage }          from './home.page';
import { ProjectsComponent } from './projects/projects.component';
import { TasksComponent }    from './tasks/tasks.component';
import { CommentsComponent } from './comments/comments.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,

    HomeRouting,
  ],
  declarations: [
    HomePage,
    ProjectsComponent,
    TasksComponent,
    CommentsComponent
  ]
})
export class HomeModule { }
