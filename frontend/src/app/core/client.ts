// libs
import { ApolloClient, createNetworkInterface } from 'apollo-client';

// app
import { AuthData } from 'angular2-token';

const networkInterface = createNetworkInterface({
  uri: 'api/edge/graphql',
  opts: {
    credentials: 'same-origin',
  },
});

// apollo client middleware
networkInterface.use([{
  applyMiddleware(req, next) {
    if (!req.options.headers) {
      req.options.headers = {};  // Create the header object if needed.
    }

    // extracted from angular2-token source
    // TODO find better way (injecter?)
    let authData: AuthData = {
        accessToken:    localStorage.getItem('accessToken'),
        client:         localStorage.getItem('client'),
        expiry:         localStorage.getItem('expiry'),
        tokenType:      localStorage.getItem('tokenType'),
        uid:            localStorage.getItem('uid')
    };

    req.options.headers["access-token"] = authData.accessToken
    req.options.headers["client"] =       authData.client
    req.options.headers["token-type"] =   authData.tokenType
    req.options.headers["expiry"] =       authData.expiry
    req.options.headers["uid"] =          authData.uid

    next();
  }
}]);

const client = new ApolloClient({ networkInterface });

export function getClient(): ApolloClient {
  return client;
}


