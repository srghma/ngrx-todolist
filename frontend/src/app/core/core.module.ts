// angular
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

// api libs
import { Angular2TokenService } from 'angular2-token';
import { ApolloModule } from 'apollo-angular';
import ApolloClient from 'apollo-client';

// other libs
import { ToastyModule } from 'ng2-toasty';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
// import { Logger, Options as LoggerOptions } from 'angular2-logger/core';

// app
import { getClient } from './client';

@NgModule({
  // modules go here
  imports: [
    HttpModule,
    RouterModule,

    ApolloModule.withClient(getClient),

    ToastyModule.forRoot(),
    SlimLoadingBarModule.forRoot(),
  ],
  // exports go here
  exports: [
    ToastyModule,
    SlimLoadingBarModule,
  ],
  // services go here
  providers: [
    Angular2TokenService,
    // Logger,
    // { provide: LoggerOptions, useValue: { level: environment.logger_level } }
  ]
})
export class CoreModule {
  // Prevent reimport of the CoreModule
  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}
