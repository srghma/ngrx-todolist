// operators needed throughout application
import './operators';

// angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule }      from '@angular/core';
import { FormsModule }   from '@angular/forms';

// app
import { CoreModule } from './core';
import { AuthModule } from './auth';
// import { HomeModule } from './home';

import { AppPage }      from './app.page';
import { NotFoundPage } from './not-found.page';
import { AppRouting }   from './app.routing';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,

    CoreModule,
    // HomeModule,
    AuthModule,
    AppRouting // must be imported last
  ],
  declarations: [
    AppPage,
    NotFoundPage
  ],
  bootstrap: [ AppPage ]
})
export class AppModule { }
