import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `
    <div class="not-found-error text-white">
      <div class="not-found-error-text text-center">
        <h1>404</h1>
        <h2>Not found</h2>
      </div>
    </div>
  `
})
export class NotFoundPage { }
