// angular
import { Component } from '@angular/core';
import { Router } from '@angular/router';

// libs
import { Angular2TokenService } from 'angular2-token';

// app
import { SIGN_UP_FORM } from '../auth.forms'

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss']
})
export class SignUpPage {
  signUpForm = SIGN_UP_FORM;
  errors: string[];

  constructor(
    private tokenService: Angular2TokenService,
    private router: Router,
  ) { }

  onSubmit({ email, password }): void {
    this.errors = null;
    this.tokenService
      .registerAccount({ email, password, passwordConfirmation: password })
      .subscribe(
        ()     => { this.router.navigate(['/']) },
        errors => { this.errors = errors.json().errors.full_messages.slice(0, 1) }
      );
  }
}
