// angular
import { Component } from '@angular/core';
import { Router } from '@angular/router';

// libs
import { Angular2TokenService } from 'angular2-token';

// app
import { SIGN_IN_FORM } from '../auth.forms'

@Component({
  selector:    'app-auth-sign-in',
  templateUrl: './sign-in.page.html',
  styleUrls:   ['./sign-in.page.scss']
})
export class SignInPage {
  signInForm = SIGN_IN_FORM;
  errors: string[];

  constructor(
    private tokenService: Angular2TokenService,
    private router: Router
  ) { }

  onSubmit({ email, password }): void {
    this.errors = null;
    this.tokenService
      .signIn({ email, password })
      .subscribe(
        ()     => { this.router.navigate(['/']) },
        errors => { this.errors = errors.json().errors }
      );
  }
}
