// angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// app
import { AuthPage } from './auth.page';
import { SignInPage } from './sign-in/sign-in.page';
import { SignUpPage } from './sign-up/sign-up.page';

const authRoutes: Routes = [{
  path: 'auth',
  component: AuthPage,
  children: [
    { path: 'sign-in', component: SignInPage },
    { path: 'sign-up', component: SignUpPage },
  ]
}];

@NgModule({
  imports: [
    RouterModule.forChild(authRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AuthRouting { }
