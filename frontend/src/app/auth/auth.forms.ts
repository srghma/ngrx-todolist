export interface IBaseField {
  key:   string;
  id:    string;
  type:  string;
  label: string;
  rules: string;
}

export const SIGN_IN_FORM: IBaseField[] = [{
  key:   'email',
  id:    'email',
  type:  'email',
  label: 'Email address',
  rules: 'required|email'
}, {
  key:   'password',
  id:    'password',
  type:  'password',
  label: 'Password',
  rules: 'required|rangeLength:8,50'
}];

export const SIGN_UP_FORM: IBaseField[] = [{
  key:   'email',
  id:    'email',
  type:  'email',
  label: 'Email address',
  rules: 'required|email'
}, {
  key:   'password',
  id:    'password',
  type:  'password',
  label: 'Password',
  rules: 'required|rangeLength:8,50'
}];

export const RESET_PASSWORD_FORM: IBaseField[] = [{
  key:   'email',
  id:    'email',
  type:  'email',
  label: 'Email address',
  rules: 'required|email'
}];

export const UPDATE_PASSWORD_FORM: IBaseField[] = [{
  key:   'password',
  id:    'password',
  type:  'password',
  label: 'Password',
  rules: 'required|rangeLength:8,50'
}, {
  key:   'passwordConfirmation',
  id:    'password_confirmation',
  type:  'password',
  label: 'Password confirmation',
  rules: 'required|equalTo:password'
}];
