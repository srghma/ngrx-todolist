import { Component } from '@angular/core';

@Component({
  selector: 'app-auth-header',
  template: `
    <h3 class="card-header"><ng-content></ng-content></h3>
  `,
  styles: []
})
export class AuthHeaderComponent {
  constructor() { }
}
