import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-auth-errors',
  template: `
    <div *ngIf="errors" class="alert alert-danger">
      <h5>Error!</h5>
      <p *ngFor="let error of errors">{{error}}</p>
    </div>
  `,
  styles: []
})
export class AuthErrorsComponent {
  @Input() errors: string[];
  constructor() { }
}

