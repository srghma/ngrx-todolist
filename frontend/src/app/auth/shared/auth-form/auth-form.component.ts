// angular
import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

// libs
import { ValidationManager } from 'ng2-validation-manager';

// app
import { IBaseField } from '../../auth.forms';

@Component({
  selector: 'app-auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.sass']
})
export class AuthFormComponent implements OnInit {
  public formManager;
  public form;
  @Input()  formStructure: IBaseField[];
  @Output() formSubmit = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
    this.formManager = new ValidationManager(this.sliceRules());
    this.form = this.formManager.getForm();
  }

  submitToParent() {
    this.formSubmit.emit(this.form.value);
  }

  fieldIsInvalid(fieldType: string): boolean {
    return this.form.controls[fieldType].touched &&
           this.formManager.hasError(fieldType);
  }

  private sliceRules() {
    const buffer = {}
    this.formStructure.forEach(field => buffer[field.key] = field.rules);
    return buffer;
  }
}
