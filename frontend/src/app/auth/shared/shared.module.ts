import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AuthErrorsComponent } from './auth-errors/auth-errors.component';
import { AuthHeaderComponent } from './auth-header/auth-header.component';
import { AuthLinksComponent } from './auth-links/auth-links.component';
import { AuthFormComponent } from './auth-form/auth-form.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule
  ],
  declarations: [
    AuthErrorsComponent,
    AuthHeaderComponent,
    AuthFormComponent,
    AuthLinksComponent,
  ],
  exports: [
    AuthErrorsComponent,
    AuthHeaderComponent,
    AuthFormComponent,
    AuthLinksComponent
  ]
})
export class SharedModule { }
