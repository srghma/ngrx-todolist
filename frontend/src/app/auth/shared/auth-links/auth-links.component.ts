import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-auth-links',
  template: `
    <p *ngIf="case != 'reset-password'"><small><a routerLink="/auth/reset-password">Forgot Password?</a></small></p>
    <p *ngIf="case != 'sign-up'"><small><a routerLink="/auth/sign-up">Sign Up</a></small></p>
    <p *ngIf="case != 'sign-in'"><small><a routerLink="/auth/sign-in">Sign In</a></small></p>
  `,
  styles: []
})
export class AuthLinksComponent {
  @Input() case: string;
}
