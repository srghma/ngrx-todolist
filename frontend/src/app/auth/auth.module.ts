// angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

// app
import { SharedModule } from './shared/shared.module';
import { AuthPage } from './auth.page';
import { AuthRouting } from './auth.routing';
import { SignInPage } from './sign-in/sign-in.page';
import { SignUpPage } from './sign-up/sign-up.page';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,

    SharedModule,
    AuthRouting
  ],
  declarations: [
    AuthPage,
    SignInPage,
    SignUpPage,
  ]
})
export class AuthModule { }
