import { Level as LoggerLevel } from 'angular2-logger/core'

export const environment = {
  production: false,
  logger_level: LoggerLevel.ERROR
};

