/* tslint:disable */
//  This file was automatically generated and should not be edited.

export type CreateProjectMutationVariables = {
  name: string,
};

export type CreateProjectMutation = {
  createProject:  {
    project:  {
      name: string,
    } | null,
  } | null,
};

export type DestroyProjectMutationVariables = {
  id: string,
};

export type DestroyProjectMutation = {
  // Destroy a Project by id
  destroyProject:  {
    project:  {
      name: string,
    } | null,
  } | null,
};

export type ProjectsQuery = {
  projects:  {
    // A list of edges.
    edges:  Array< {
      // The item at the end of the edge.
      node:  {
        name: string,
        id: number,
      } | null,
    } > | null,
  } | null,
};

export type ReadTodolistQuery = {
  projects:  {
    // A list of edges.
    edges:  Array< {
      // The item at the end of the edge.
      node:  {
        name: string,
        id: number,
        tasks:  {
          // A list of edges.
          edges:  Array< {
            // The item at the end of the edge.
            node:  {
              id: number,
              name: string,
              done: boolean | null,
              deadline: string | null,
              position: number | null,
              comments:  {
                // A list of edges.
                edges:  Array< {
                  // The item at the end of the edge.
                  node:  {
                    id: number,
                    text: string,
                    attachments:  {
                      // A list of edges.
                      edges:  Array< {
                        // The item at the end of the edge.
                        node:  {
                          id: number,
                          file: string | null,
                        } | null,
                      } > | null,
                    },
                  } | null,
                } > | null,
              },
            } | null,
          } > | null,
        },
      } | null,
    } > | null,
  } | null,
};

export type UpdateProjectMutationVariables = {
  id: string,
  name: string,
};

export type UpdateProjectMutation = {
  updateProject:  {
    project:  {
      id: number,
      name: string,
    } | null,
  } | null,
};
/* tslint:enable */
